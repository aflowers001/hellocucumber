package com.flatcat.hellocucumber;

public class IsItFridayService {

    static String isItFriday(String today) {
        return "Friday".equals(today) ? "Yep":"Nope";
    }

}
