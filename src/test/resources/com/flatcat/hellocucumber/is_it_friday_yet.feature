Feature: Is it Friday yet?
  Everybody wants to know when it's Friday

  Scenario Outline: Today is or isn't Friday
    Given today is "<day>"
    When I ask whether it's Friday yet
    Then I should be told "<expectedAnswer>"

    Examples:
      | day       | expectedAnswer |
      | Monday    | Nope           |
      | Tuesday   | Nope           |
      | Wednesday | Nope           |
      | Thursday  | Nope           |
      | Friday    | Yep            |
      | Saturday  | Nope           |
      | Sunday    | Nope           |
      | Not a day | Nope           |